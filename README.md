# Betting project

## Tech-set:
-   Angular2
-   Express JS
-   MongoDB(Free Atlas cluster)

## Set-up:
-   Front-end:
    -   `./betting-project-client`
        -   `npm install`
        -   Create config file `./betting-project-client/src/config.json`
            -   ``` json
                {
                    "baseUrl": {API_HOST}
                }
                ```
            -   `API_HOST`: Your API host address
        -   `npm start`
-   Back-end:
    -   `./betting-project-api`
        -   `npm install`
        -   Create config file `./betting-project-api/config.json`
            -   ``` json
                {
                    "development": {
                        "node_port": {SERVER_PORT},
                        "database": {MONGODB_ATLAS_HOST}
                    },
                    "testing": {
                        "database": {MONGODB_ATLAS_HOST}
                    },
                    "production": {
                        "node_port": {SERVER_PORT},
                        "database": {MONGODB_ATLAS_HOST}
                    }
                }

                ```
            -   `SERVER_PORT`: Your server port
            -   `MONGODB_ATLAS_HOST`: Your mongoDB host(or connection string)
                -    Example: `mongodb+srv://<DB_USERNAME>:<DB_PASSWORD>@<CLUSTER_NAME>.mongodb.net/test?retryWrites=true&w=majority`

## Back-end docs:
-   Endpoints:
    -   `GET /events`
        -   Response body: `Array with all Event Objects`
    -   `POST /events`
        -   Request body: `{}`
        -   Response body: `Created Event object containing only generated id and preset startDate`
    -   `DELETE /events/:eventId`
        -   Request body: `{}`
        -   Response body: `The deleted Event from the database if found`
            -   404 body:
            ```js
            {
                error: string,
                code: number,
                message: string
            }
            ```
        -   Response codes: 
            -   If Event is found and deleted: `200`
            -   If Event with `eventId` not found: `404`
    -   `PATCH /events/:eventId`
        -   Request body: All properties are optional. If provided, they overwrite properties in DB
        ```js
        {
            name: string,
            oddsFirst: string,
            oddsDraw: string,
            oddsSecond: string,
            startDate: string
        }
        ```
        -   Response body: `The updated Event from the database if found`
            -   404 body:
            ```js
            {
                error: string,
                code: number,
                message: string
            }
            ```
        -   Response codes: 
            -   If Event is found and updated: `200`
            -   If Event with `eventId` not found: `404`
-   Models:
    -   EventModel:
    ``` js
    Event {
        id: string,
        name: string,
        oddsFirst: string,
        oddsDraw: string,
        oddsSecond: string,
        startDate: string
    }
    ```
    -   DateFormat: `YYYY-MM-DDTHH:MM`
