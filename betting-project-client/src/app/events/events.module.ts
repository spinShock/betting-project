import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ConfigService } from '../config/config.service';
import { EventsService } from './events.service';
import { EventsComponent } from './events.component';

@NgModule({
  declarations: [EventsComponent],
  imports: [BrowserModule, ReactiveFormsModule],
  providers: [
    ConfigService,
    EventsService,
  ],
  exports: [EventsComponent],
})
export class EventsModule {}
