import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { EventsService } from './events.service';
import { IEvent } from '../models/event.interface';
import { greaterEqualThanValidator } from '../common/directives/greater-equal-than.directive';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
  public events: IEvent[];
  public editMode: boolean;
  public eventForm: FormGroup;

  constructor(
    private readonly fb: FormBuilder,
    private readonly eventsService: EventsService
    ) {
    this.editMode = false;
  }

  ngOnInit() {
    this.eventForm = this.fb.group({
      events: this.fb.array([])
    })
    this.eventsService.getEvents().subscribe((events: IEvent[]) => {
      this.events = events;
      this.createForms(events);
    })
  }

  public isEventExpired(event: IEvent): boolean {
    return new Date() > new Date(event.startDate);
  }

  public toggleEdit() {
    this.editMode = !this.editMode;
    this.createForms(this.events);
  }

  public deleteEvent(id: string) {
      this.eventsService.deleteEvent(id).subscribe((deletedEvent: IEvent[]) => {
        this.events = this.events.filter((event: IEvent) => event.id !== deletedEvent[0].id)
      });
  }

  public addNew() {
    this.eventsService.addEvent().subscribe((addedEvent: IEvent[]) => {
      this.events.push(addedEvent[0]);
      this.createForm(addedEvent[0]);
    });
  }

  public updateEvent(id: string, i: number) {
    if ((this.eventForm.get('events') as FormArray).controls[i].valid) {
      const toBeUpdatedEvent: IEvent = this.getUpdatedEvent(id, i);
      this.eventsService.updateEvent(toBeUpdatedEvent).subscribe((updatedEvent: IEvent[]) => {
        this.events[i] = updatedEvent[0];
      });
    }
  }

  private createForms(events: IEvent[]) {
    (this.eventForm.get('events') as FormArray).controls = [];
    events.forEach((event: IEvent) => {
      (this.eventForm.get('events') as FormArray).push(this.fb.group({
        name: [event.name, Validators.required],
        oddsFirst: [event.oddsFirst ? (+event.oddsFirst).toFixed(2) : '',  [Validators.required, Validators.pattern(/^[.\d]+$/), greaterEqualThanValidator(1), Validators.maxLength(15)]],
        oddsDraw: [event.oddsDraw ? (+event.oddsDraw).toFixed(2) : '',  [Validators.required, Validators.pattern(/^[.\d]+$/), greaterEqualThanValidator(1), Validators.maxLength(15)]],
        oddsSecond: [event.oddsSecond ? (+event.oddsSecond).toFixed(2) : '',  [Validators.required, Validators.pattern(/^[.\d]+$/), greaterEqualThanValidator(1), Validators.maxLength(15)]],
        startDate: [event.startDate, Validators.required],
      }))
    })
  }

  private createForm(event: IEvent) {
    (this.eventForm.get('events') as FormArray).controls
    .push(this.fb.group({
      name: [event.name, Validators.required],
      oddsFirst: [event.oddsFirst ? (+event.oddsFirst).toFixed(2) : '',  [Validators.required, Validators.pattern(/^[.\d]+$/), greaterEqualThanValidator(1), Validators.maxLength(15)]],
      oddsDraw: [event.oddsDraw ? (+event.oddsDraw).toFixed(2) : '',  [Validators.required, Validators.pattern(/^[.\d]+$/), greaterEqualThanValidator(1), Validators.maxLength(15)]],
      oddsSecond: [event.oddsSecond ? (+event.oddsSecond).toFixed(2) : '',  [Validators.required, Validators.pattern(/^[.\d]+$/), greaterEqualThanValidator(1), Validators.maxLength(15)]],
      startDate: [event.startDate, Validators.required],
    }))
  }

  private getUpdatedEvent(id: string, index: number): IEvent {
    const controls = (this.eventForm.get('events') as FormArray).controls[index];
    return {
      id,
      name: controls.get('name').value,
      oddsFirst: String(+controls.get('oddsFirst').value),
      oddsDraw: String(+controls.get('oddsDraw').value),
      oddsSecond: String(+controls.get('oddsSecond').value),
      startDate: controls.get('startDate').value,
    }
  }
}
