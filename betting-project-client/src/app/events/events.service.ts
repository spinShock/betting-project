import { ConfigService } from './../config/config.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IEvent } from '../models/event.interface';

@Injectable({
    providedIn: 'root',
})
export class EventsService {
    constructor(private readonly http: HttpClient, private readonly configService: ConfigService) {}

    getEvents() {
        return this.http.get(`${this.configService.baseUrl}/events`);
    }

    deleteEvent(id: string) {
        return this.http.delete(`${this.configService.baseUrl}/events/${id}`);
    }

    updateEvent(updatedEvent: IEvent) {
      return this.http.patch(`${this.configService.baseUrl}/events/${updatedEvent.id}`, updatedEvent);
    }

    addEvent() {
      return this.http.post(`${this.configService.baseUrl}/events`, {});
    }
}
