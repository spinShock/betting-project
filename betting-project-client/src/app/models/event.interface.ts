export interface IEvent {
    id: string;
    name: string;
    oddsFirst: string;
    oddsDraw: string;
    oddsSecond: string;
    startDate: string;
}
