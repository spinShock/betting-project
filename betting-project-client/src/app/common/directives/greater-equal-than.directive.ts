import { ValidatorFn, AbstractControl } from '@angular/forms';

export function greaterEqualThanValidator(greaterThan: number): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const value = +control.value;
    return value < greaterThan || Number.isNaN(value)
    ? { greaterThanOrEqual: {value: control.value} }
    : null;
  };
}
