import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../../config/config.service';
import { map, catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

export const load = (http: HttpClient, config: ConfigService) => {
    return (): Promise<boolean> => {
        return new Promise<boolean>((resolve: (a: boolean) => void): void => {
            http.get('./config.json').pipe(
                map((x: ConfigService) => {
                    config.baseUrl = x.baseUrl;
                    resolve(true);
                }),
                catchError((x: {status: number}, caught: Observable<void>) => {
                    if (x.status !== 404) {
                        resolve(false);
                    }
                    config.baseUrl = 'http://localhost:3000';
                    resolve(true);
                    return of({});
                })
            ).subscribe();
        })
    }
}
