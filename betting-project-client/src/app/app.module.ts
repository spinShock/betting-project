import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { EventsModule } from './events/events.module';
import { AppComponent } from './app.component';
import { ConfigService } from './config/config.service';
import { load } from './common/constants/load';
import { httpInterceptorProviders } from './http-interceptors';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, HttpClientModule, EventsModule],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: load,
      multi: true,
      deps: [HttpClient, ConfigService]
    },
    ConfigService,
    // httpInterceptorProviders,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
