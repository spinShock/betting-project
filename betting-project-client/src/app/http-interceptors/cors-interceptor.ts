import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class CorsInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {
    req = req.clone({
        headers: req.headers
            .append('Access-Control-Allow-Origin', '*')
            .append('Access-Control-Allow-Headers', 'content-type')
            .append('Content-Type', 'application/json;charset=utf-8')
        })
    return next.handle(req);
  }
}