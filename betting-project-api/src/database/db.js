import { MongoClient } from 'mongodb';
import assert from 'assert';

export const clientConnect = async () => {
  const client = await (() =>
    new Promise((resolve, reject) =>
      MongoClient.connect(
        global.gConfig.database,
        { useUnifiedTopology: true },
        (err, client) => {
          // console.log(err);
          assert.equal(null, err);
          resolve(client);
        }
      )
    ))();
  return client;
};

export const getEvents = async () =>
  await (async () =>
    new Promise(async (resolve, reject) => {
      const client = await clientConnect();
      const db = client.db('data');
      const events = await db
        .collection('events')
        .find({})
        .project({ _id: 0 })
        .toArray();
      client.close();
      resolve(events);
    }))();

export const deleteEvent = async eventId =>
  await (async () =>
    new Promise(async (resolve, reject) => {
      const client = await clientConnect();
      const db = client.db('data');
      const deletedEvent = await db
        .collection('events')
        .findOneAndDelete({ id: eventId }, { projection: { _id: 0 } });
      console.log(eventId, deletedEvent);

      client.close();
      resolve([deletedEvent.value]);
    }))();

export const updateEvent = async toUpdateEvent =>
  await (async () =>
    new Promise(async (resolve, reject) => {
      const client = await clientConnect();
      const db = client.db('data');
      const updatedEvent = await db
        .collection('events')
        .findOneAndUpdate(
          { id: toUpdateEvent.id },
          { $set: toUpdateEvent },
          { returnOriginal: false }
        );
      client.close();
      resolve([updatedEvent.value]);
    }))();

export const addEvent = async toAddEvent =>
  await (async () =>
    new Promise(async (resolve, reject) => {
      const client = await clientConnect();
      const db = client.db('data');
      const newEventId = (
        await db
          .collection('events_counter')
          .findOneAndUpdate(
            { note: 'UNIQUE COUNT DOCUMENT IDENTIFIER' },
            { $inc: { count: 1 } },
            { returnOriginal: false }
          )
      ).value.count;
      toAddEvent.id = String(newEventId);
      const addedEventId = (await db.collection('events').insertOne(toAddEvent))
        .insertedId;
      const addedEvent = await db.collection('events').findOne({
        _id: addedEventId
      });
      client.close();
      resolve([addedEvent]);
    }))();
