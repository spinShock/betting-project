import config from '../config.json';

const env = process.env.NODE_ENV || 'development';
global.gConfig = config[env];
console.log(`Config: Environment set to ${env}`);
