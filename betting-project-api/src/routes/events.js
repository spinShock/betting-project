import express from 'express';
import moment from 'moment';
import { getEvents, deleteEvent, updateEvent, addEvent } from '../database/db';
const router = express.Router();
// Request interceptor to deserialize request body
const deserializeMiddleware = (req, res, next) => {
  req.body = {
    name: req.body.name,
    odds_first: req.body.oddsFirst,
    odds_draw: req.body.oddsDraw,
    odds_second: req.body.oddsSecond,
    start_date: req.body.startDate
  };
  next();
};

router.patch('/:eventId', deserializeMiddleware);

// Response interceptor to sanitize response body
router.use(function(req, res, next) {
  if (res.statusCode === 404) {
    next();
    return;
  }
  const resSend = res.send;
  res.send = function(data) {
    if (data.code !== 404) {
      const sanitizedResponse = data.map(event => ({
        id: event.id,
        name: event.name,
        oddsFirst: event.odds_first,
        oddsDraw: event.odds_draw,
        oddsSecond: event.odds_second,
        startDate: event.start_date
      }));
      res.send = resSend;
      resSend.apply(this, [sanitizedResponse]);
      return;
    }
    res.send = resSend;
    resSend.apply(this, arguments);
  };
  next();
});

// events
router.get('/', async (req, res, next) => {
  try {
    const events = await getEvents();
    res.send(events);
  } catch (error) {
    console.log(error);
  }
});

router.post('/', async (req, res, next) => {
  try {
    const date = moment()
      .hour(23)
      .minute(59)
      .format('YYYY-MM-DDTHH:mm');
    const newEvent = {
      start_date: date
    };
    const addedEvent = await addEvent(newEvent);
    res.send(addedEvent);
  } catch (error) {
    console.log(error);
  }
});

router.delete('/:eventId', async (req, res, next) => {
  try {
    const eventId = req.params.eventId;
    if (!eventId) {
      res.send({ msg: 'No id provided' });
      return;
    }
    const deletedEvent = await deleteEvent(eventId);
    if (!deletedEvent[0]) {
      res.status(404).send({
        error: 'Not found',
        code: 404,
        message: `Event with id ${eventId} not found.`
      });
      return;
    }
    res.send(deletedEvent);
  } catch (error) {
    console.log(error);
    res.status(404).send(error);
  }
});

router.patch('/:eventId', async (req, res, next) => {
  try {
    const eventId = req.params.eventId;
    if (!eventId) {
      res.send({ msg: 'No id provided' });
      return;
    }
    const toUpdateEvent = req.body;
    toUpdateEvent.id = eventId;
    const updatedEvent = await updateEvent(toUpdateEvent);
    if (!updatedEvent[0]) {
      res.status(404).send({
        error: 'Not found',
        code: 404,
        message: `Event with id ${eventId} not found.`
      });
      return;
    }
    res.send(updatedEvent);
  } catch (error) {
    console.log(error);
    res.status(404).send(error);
  }
});

export default router;
